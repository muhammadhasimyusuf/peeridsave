<?php

use App\Http\Controllers\PeersController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    // return ['Laravel' => app()->version()];
    $url = request()->getScheme() . '://' . request()->getHost() . ':5173';
    return redirect($url);
});


require __DIR__.'/auth.php';
