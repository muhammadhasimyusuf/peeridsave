<?php

namespace App\Http\Controllers;

use App\Models\Location;
use App\Models\Peers;
use Illuminate\Http\Request;

class LocationController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $exist = Location::where('location_name', $request->location_name)->first();

        if(!$exist) {
            Location::truncate();
            Peers::truncate();
            $created = Location::create([
                'location_name' => $request->location_name
            ]);
    
            return response()->json([
                'success' => true,
                'message' => 'Destroy All and create new Location',
                'result' => $created
            ], 201);
        }

        $updated = $exist->update([
            'location_name' => $request->location_name
        ]);

        return response()->json([
            'success' => true,
            'message' => 'Updated',
            'result' => $updated
        ], 201);
    }

    /**
     * Display the specified resource.
     */
    public function show(Location $location)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Location $location)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Location $location)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Location $location)
    {
        //
    }
}
