<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('peers', function (Blueprint $table) {
            $table->json('setting')->nullable(); // Adding a new JSON field
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('peers', function (Blueprint $table) {
            $table->dropColumn('setting'); // Dropping the JSON field
        });
    }
};
